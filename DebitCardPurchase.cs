using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCardPurchase
    {
        public DebitCardPurchase()
        {
            this.DebitCardOrders = new List<DebitCardOrder>();
        }

        public int id { get; set; }
        public int card_id { get; set; }
        public Nullable<System.DateTime> purchased_on { get; set; }
        public string inmate_name { get; set; }
        public byte[] customer_photo { get; set; }
        public string purchase_method { get; set; }
        public string trans_ID { get; set; }
        public Nullable<int> bill_cnt { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> handling_fee { get; set; }
        public Nullable<decimal> surcharge { get; set; }
        public Nullable<int> terminate_code { get; set; }
        public string terminate_text { get; set; }
        public Nullable<int> credit_authorization_id { get; set; }
        public Nullable<int> refund_state { get; set; }
        public int facility_id { get; set; }
        public Nullable<int> purchased_from_id { get; set; }
        public virtual DebitCard DebitCard { get; set; }
        public virtual ICollection<DebitCardOrder> DebitCardOrders { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }
    }
}
