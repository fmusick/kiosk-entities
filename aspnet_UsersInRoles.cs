using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class aspnet_UsersInRoles
    {
        public System.Guid UserId { get; set; }
        public System.Guid RoleId { get; set; }
        public virtual aspnet_Roles aspnet_Roles { get; set; }
        public virtual aspnet_Roles aspnet_Roles1 { get; set; }
        public virtual aspnet_Roles aspnet_Roles2 { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual aspnet_Users aspnet_Users1 { get; set; }
        public virtual aspnet_Users aspnet_Users2 { get; set; }
    }
}
