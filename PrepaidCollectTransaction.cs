using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class PrepaidCollectTransaction
    {
        public int id { get; set; }
        public int Type { get; set; }
        public decimal amount { get; set; }
        public System.DateTime DepositedOn { get; set; }
        public string PurchaseMethod { get; set; }
        public string TransactionId { get; set; }
        public Nullable<int> BillCount { get; set; }
        public decimal HandlingFee { get; set; }
        public decimal surcharge { get; set; }
        public string CreditAuthorization { get; set; }
        public Nullable<System.DateTime> CreditTransactionDate { get; set; }
        public string CreditTransactionId { get; set; }
        public Nullable<decimal> CreditTransactionAmount { get; set; }
        public Nullable<decimal> CreditTransactionFee { get; set; }
        public string UserName { get; set; }

        //public string Scidyn.Models.Facility Facility { get; set; }

        public string Outlet { get; set; }
        public string Phone { get; set; }
        public string CallerName { get; set; }
        public string CallerEmail { get; set; }
        public string CallerPhone { get; set; }
    }
}
