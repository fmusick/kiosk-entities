using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class NexusInstance
    {
        public NexusInstance() { }

        //{
        //    this.Facilities = new List<Scidyn.Models.Facility>();
        //}

        public int id { get; set; }
        public string name { get; set; }
        public string server_id { get; set; }
        public string login { get; set; }
        public string password { get; set; }

        //public virtual ICollection<Scidyn.Models.Facility> Facilities { get; set; }
    }
}
