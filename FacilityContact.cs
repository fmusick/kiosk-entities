using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class FacilityContact
    {
        public int id { get; set; }
        public int facility_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

        public virtual Facility Facility { get; set; }

    }
}
