using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kiosk.Models
{
    public partial class Fee
    {
        public int id { get; set; }
        public int facility_id { get; set; }
        public decimal maximum_amount { get; set; }
        public decimal minimum_amount { get; set; }
        public decimal prepaidcard_percent_fee { get; set; }
        public decimal pindeposit_percent_fee { get; set; }
        public decimal directpay_percent_fee { get; set; }
        public decimal prepaidcard_flat_fee { get; set; }
        public decimal pindeposit_flat_fee { get; set; }
        public decimal directpay_flat_fee { get; set; }
        public bool prepaidcard_flag { get; set; }
        public bool pindeposit_flag { get; set; }
        public bool directpay_flag { get; set; }
        public decimal commissarydeposit_percent_fee { get; set; }
        public decimal commissarydeposit_flat_fee { get; set; }
        public bool commissarydeposit_flag { get; set; }
        public decimal commissary_min { get; set; }
        public decimal commissary_max { get; set; }
        public bool prepaidcard_active { get; set; }
        public bool pindeposit_active { get; set; }
        public bool directpay_active { get; set; }
        public bool commissarydeposit_active { get; set; }
        public decimal prepaidcard_max { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }
    }
}
