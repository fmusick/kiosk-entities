using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class TrustDepositOrder
    {
        public int id { get; set; }
        public Nullable<System.Guid> aspnet_Users_UserId { get; set; }
        public Nullable<int> TrustDeposit_id { get; set; }
        public Nullable<bool> Sync { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual TrustDeposit TrustDeposit { get; set; }
    }
}
