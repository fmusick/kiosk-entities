using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class PINDebitOrder
    {
        public int id { get; set; }
        public System.Guid aspnet_Users_UserId { get; set; }
        public int PINDebitDeposit_id { get; set; }
        public string CallerFirstName { get; set; }
        public string CallerLastName { get; set; }
        public string CallerEmail { get; set; }
        public string CallerPhone { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual PINDebitDeposit PINDebitDeposit { get; set; }
    }
}
