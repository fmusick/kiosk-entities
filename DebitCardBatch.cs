using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCardBatch
    {
        public enum DebitCardBatchStatus
        {
            NORMAL,
            DELETED,
            LOCKED
        }

        public DebitCardBatch()
        {
            this.DebitCardBatchHistoryItems = new List<DebitCardBatchHistoryItem>();
        }

        public int id { get; set; }
        public System.DateTime created { get; set; }
        public string MMbatchID { get; set; }
        public string cardType { get; set; }
        public int seqMin { get; set; }
        public int seqMax { get; set; }
        public int cardCount { get; set; }
        public string title { get; set; }
        public string subtitle { get; set; }
        public string instructions { get; set; }
        public string disclaimer { get; set; }
        public bool enabled { get; set; }
        public System.DateTime activationDate { get; set; }
        public Nullable<System.DateTime> expirationDate { get; set; }
        public Nullable<int> expInXDays { get; set; }
        public Nullable<int> minuteAmt { get; set; }
        public decimal dollarAmount { get; set; }
        public Nullable<int> callsAllowed { get; set; }
        public bool units { get; set; }
        public string status { get; set; }
        public int facility_id { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }

        public virtual ICollection<DebitCardBatchHistoryItem> DebitCardBatchHistoryItems { get; set; }

        public virtual ICollection<DebitCard> DebitCards { get; set; }
    }
}
