using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class TrustDepositTransaction
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime DepositedOn { get; set; }
        public string PurchaseMethod { get; set; }
        public string TransactionId { get; set; }
        public Nullable<int> BillCount { get; set; }
        public decimal HandlingFee { get; set; }
        public decimal Surcharge { get; set; }
        public string CreditAuthorization { get; set; }
        public Nullable<System.DateTime> CreditTransactionDate { get; set; }
        public string CreditTransactionId { get; set; }
        public Nullable<decimal> CreditTransactionAmount { get; set; }
        public Nullable<decimal> CreditTransactionFee { get; set; }
        public string Inmate { get; set; }
        public string UserName { get; set; }

        // public string Scidyn.Models.Facility { get; set; }

        public string Outlet { get; set; }
    }
}
