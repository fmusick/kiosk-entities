using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class KioskUpdateSite
    {
        public int rec_id { get; set; }
        public string site_id { get; set; }
        public string user { get; set; }
        public string site_name { get; set; }
        public string dataroot { get; set; }
        public string fileroot { get; set; }
        public string filemask { get; set; }
        public string fileext { get; set; }
        public int templife { get; set; }
        public bool docid_is_key { get; set; }
        public string root_tag { get; set; }
        public string record_tag { get; set; }
        public string pin_tag { get; set; }
        public string docid_tag { get; set; }
        public string fname_tag { get; set; }
        public string mname_tag { get; set; }
        public string lname_tag { get; set; }
        public string dob_tag { get; set; }
        public string ui_tag { get; set; }
        public string lname_fname_tag { get; set; }
        public string fname_lname_tag { get; set; }
        public string action_tag { get; set; }
        public string action_attribute { get; set; }
        public string add_tag { get; set; }
        public string add_text { get; set; }
        public string remove_tag { get; set; }
        public string remove_text { get; set; }
        public string balance_tag { get; set; }
        public string site_tag { get; set; }
        public Nullable<decimal> balance_adjust { get; set; }
        public Nullable<int> kiosk_facility_id { get; set; }
        public string kiosk_server { get; set; }
        public string kiosk_sqluid { get; set; }
        public string kiosk_sqlpswd { get; set; }
        public string kiosk_db { get; set; }
        public bool kiosk_delete { get; set; }
        public string last_report { get; set; }
        public string report_time { get; set; }
        public string email_to { get; set; }
        public string email_cc { get; set; }
        public string alert_email { get; set; }
        public Nullable<System.DateTime> last_file { get; set; }
        public Nullable<System.DateTime> last_update { get; set; }
        public int warn_limit { get; set; }
        public int warn_window { get; set; }
        public int file_wait { get; set; }
        public Nullable<System.DateTime> last_warning { get; set; }
        public bool append_site_to_ui { get; set; }
        public string location_tag { get; set; }
    }
}
