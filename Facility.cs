using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class Facility
    {
        public Facility()
        {
            this.FacilityAddresses = new List<FacilityAddress>();
            this.FacilityContacts = new List<FacilityContact>();
        }

        public int id { get; set; }

        public string name { get; set; }

        public string site_id { get; set; }

        public string cic { get; set; }

        public string commissary_data { get; set; }

        public int nexus_instance_id { get; set; }

        public virtual ICollection<FacilityAddress> FacilityAddresses { get; set; }

        public virtual ICollection<FacilityContact> FacilityContacts { get; set; }
    }
}
