using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class BailDeposit
    {
        public int id { get; set; }
        public Nullable<decimal> amount { get; set; }
        public System.DateTime deposited_on { get; set; }
        public byte[] customer_photo { get; set; }
        public string purchase_method { get; set; }
        public string trans_ID { get; set; }
        public Nullable<int> bill_cnt { get; set; }
        public decimal handling_fee { get; set; }
        public decimal surcharge { get; set; }
        public int terminate_code { get; set; }
        public string terminate_text { get; set; }
        public int refund_state { get; set; }
        public int inmatebail_id { get; set; }
        public Nullable<int> credit_authorization_id { get; set; }
        public int facility_id { get; set; }
        public Nullable<int> purchased_from_id { get; set; }
        public virtual Kiosk Kiosk { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }

        public virtual InmateBail InmateBail { get; set; }
        public virtual CreditCardTransaction CreditCardTransaction { get; set; }
    }
}
