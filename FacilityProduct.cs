using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class FacilityProduct
    {
        public FacilityProduct()
        {
            this.FacilityProductFees = new List<FacilityProductFee>();
        }

        public int id { get; set; }

        public int facility_id { get; set; }
        public int product_id { get; set; }
        public bool enabled { get; set; }
        public bool allow_inmate_search { get; set; }
        public decimal amount { get; set; }
        public int quantity_maximum { get; set; }
        public int quantity_minimum { get; set; }
        public string report_email { get; set; }

        // SQL tinyint maps to byte, SQL smallint maps to Int16, SQL int maps to Int32, SQL bigint maps to Int64
        public Nullable<Int16> report_time { get; set; }

        public string receipt_email { get; set; }
        public Nullable<int> outlet_id { get; set; }
        public Nullable<int> payment_gateway_type_id { get; set; }

        //public virtual Scidyn.Models.Facility Facility { get; set; }

        public virtual Product Product { get; set; }

        public virtual ICollection<FacilityProductFee> FacilityProductFees { get; set; }
    }
}
