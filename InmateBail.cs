using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class InmateBail
    {
        public InmateBail()
        {
            this.BailDeposits = new List<BailDeposit>();
        }

        public int id { get; set; }
        public decimal bail_amount { get; set; }
        public decimal fee { get; set; }
        public int incarceration_id { get; set; }
        public virtual ICollection<BailDeposit> BailDeposits { get; set; }
        public virtual Incarceration Incarceration { get; set; }
    }
}
