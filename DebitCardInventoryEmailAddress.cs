using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCardInventoryEmailAddress
    {
        public int id { get; set; }

        public string email  { get; set; }
    }
}
