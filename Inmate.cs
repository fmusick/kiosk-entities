using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kiosk.Models
{
    public partial class Inmate
    {
        public Inmate()
        {
            this.Incarcerations = new List<Incarceration>();
            this.TrustDeposits = new List<TrustDeposit>();
            this.PINDebitDeposits = new List<PINDebitDeposit>();
        }
        
        public int id { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public System.DateTime created { get; set; }
        public System.DateTime date_of_birth { get; set; }
        //public string unique_identifier { get; set; }
        //public decimal commissary_balance { get; set; }
        //public string Doc_ID { get; set; }
        //public string Location { get; set; }

        public int facility_id { get; set; }

        public int location_id { get; set; }

        public virtual ICollection<Incarceration> Incarcerations { get; set; }
        public virtual ICollection<TrustDeposit> TrustDeposits { get; set; }
        public virtual ICollection<PINDebitDeposit> PINDebitDeposits { get; set; }

        public Incarceration GetLastIncarceration()
        {
            return Incarcerations.Count == 0 ? null : (from i in this.Incarcerations orderby i.incarcerated_on descending select i).First<Incarceration>();
        }
    }
}
