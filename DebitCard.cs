using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCard
    {
        public enum DebitCardStatus
        {
            UNALLOCATED,
            ALLOCATED_TO_KIOSK,
            SOLD,
            BATCH_LOCKED,
            RESERVED
        }

        public enum DebitCardType
        {
            REWARD_CARD,
            DOLLAR_CARD,
            MINUTE_CARD,
            MINUTE_THRESHOLD_CARD,
            PEG_DEBIT,
            UNIT_CARD
        }

        public DebitCard()
        {
            this.DebitCardPurchases = new List<DebitCardPurchase>();
        }

        public int id { get; set; }
        public int batchID { get; set; }
        public string cardID { get; set; }
        public int facilityID { get; set; }
        public string MMbatchID { get; set; }
        public int seqNum { get; set; }
        public string cardType { get; set; }
        public string title { get; set; }
        public string subtitle { get; set; }
        public string instructions { get; set; }
        public string disclaimer { get; set; }
        public bool enabled { get; set; }
        public System.DateTime activationDate { get; set; }
        public Nullable<System.DateTime> expirationDate { get; set; }
        public Nullable<int> expInXDays { get; set; }
        public Nullable<int> minAmt { get; set; }
        public int centValue { get; set; }
        public Nullable<int> callsAllowed { get; set; }
        public bool units { get; set; }
        public string status { get; set; }
        public decimal value { get; set; }
        public System.DateTime imported_on { get; set; }
        public Nullable<int> kioskID { get; set; }
        public virtual ICollection<DebitCardPurchase> DebitCardPurchases { get; set; }
    }
}
