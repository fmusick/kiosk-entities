using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class FacilityInmateEvent
    {
        public int id { get; set; }
        public string activity { get; set; }
        public int inmate_id { get; set; }
        public int facility_id { get; set; }

        //public virtual Scidyn.Models.Facility Facility { get; set; }
    }
}
