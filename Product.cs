using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class Product
    {
        public Product()
        {
            this.FacilityProducts = new List<FacilityProduct>();
        }

        public int id { get; set; }
        public int product_catalogue_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public decimal value { get; set; }

        public virtual ICollection<FacilityProduct> FacilityProducts { get; set; }

    }
}
