using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class FF2XMLSites
    {
        public string site_id { get; set; }
        public string user { get; set; }
        public string site_name { get; set; }
        public string fileroot { get; set; }
        public string fileext { get; set; }
        public int templife { get; set; }
        public int name_max_length { get; set; }
        public int field_count { get; set; }
        public string seperator { get; set; }
        public string ftpsite { get; set; }
        public string ftpuid { get; set; }
        public string ftppswd { get; set; }
        public Nullable<int> ftpwait { get; set; }
        public string last_report { get; set; }
        public string report_time { get; set; }
        public string email_to { get; set; }
        public string email_cc { get; set; }
        public string alert_email { get; set; }
        public Nullable<System.DateTime> last_file { get; set; }
        public Nullable<System.DateTime> last_update { get; set; }
        public int warn_limit { get; set; }
        public int warn_window { get; set; }
        public Nullable<System.DateTime> last_warning { get; set; }
        public Nullable<int> ftpretrys { get; set; }
        public string dataroot { get; set; }
        public string xmlhead { get; set; }
        public string xmltail { get; set; }
        public string xmlparse { get; set; }
        public Nullable<int> disc_header_rows { get; set; }
        public Nullable<int> disc_footer_rows { get; set; }
    }
}
