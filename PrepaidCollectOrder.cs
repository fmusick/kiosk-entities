using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class PrepaidCollectOrder
    {
        public int id { get; set; }
        public Nullable<System.Guid> aspnet_Users_UserId { get; set; }
        public Nullable<int> PrepaidCollectDepost_id { get; set; }
        public string CallerFirstName { get; set; }
        public string CallerLastName { get; set; }
        public string CallerEmail { get; set; }
        public string CallerPhone { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual PrepaidCollectDeposit PrepaidCollectDeposit { get; set; }
    }
}
