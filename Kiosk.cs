using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class Kiosk
    {
        public Kiosk()
        {
            this.BailDeposits = new List<BailDeposit>();
            this.PrepaidCollectDeposits = new List<PrepaidCollectDeposit>();
            this.TrustDeposits = new List<TrustDeposit>();
            this.PINDebitDeposits = new List<PINDebitDeposit>();
            this.BaseEvents = new List<BaseEvent>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string secret_key { get; set; }
        public int location_id { get; set; }
        public virtual ICollection<BailDeposit> BailDeposits { get; set; }
        public virtual ICollection<PrepaidCollectDeposit> PrepaidCollectDeposits { get; set; }
        public virtual Location Location { get; set; }
        public virtual ICollection<TrustDeposit> TrustDeposits { get; set; }
        public virtual ICollection<PINDebitDeposit> PINDebitDeposits { get; set; }
        public virtual ICollection<BaseEvent> BaseEvents { get; set; }

    }
}
