using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class Location
    {
        public Location()
        {
            this.Kiosks = new List<Kiosk>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public int facility_id { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }

        public virtual ICollection<Kiosk> Kiosks { get; set; }
    }
}
