using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCardOrder
    {
        public int Id { get; set; }
        public Nullable<System.Guid> aspnet_Users_UserId { get; set; }
        public Nullable<int> DebitCardPurchase_id { get; set; }
        public string CallerFirstName { get; set; }
        public string CallerLastName { get; set; }
        public string CallerEmail { get; set; }
        public string CallerPhone { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual DebitCardPurchase DebitCardPurchase { get; set; }
    }
}
