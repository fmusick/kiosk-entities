using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class Incarceration
    {
        public Incarceration()
        {
            this.InmateBails = new List<InmateBail>();
        }

        public int id { get; set; }
        public string inmate_identifier { get; set; }
        public System.DateTime incarcerated_on { get; set; }
        public Nullable<System.DateTime> released_on { get; set; }
        public int facility_id { get; set; }
        public int inmate_id { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }

        public virtual Inmate Inmate { get; set; }
        public virtual ICollection<InmateBail> InmateBails { get; set; }
    }
}
