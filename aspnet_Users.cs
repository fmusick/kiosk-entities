using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class aspnet_Users
    {
        public aspnet_Users()
        {
            this.aspnet_PersonalizationPerUser = new List<aspnet_PersonalizationPerUser>();
            this.aspnet_PersonalizationPerUser1 = new List<aspnet_PersonalizationPerUser>();
            this.aspnet_PersonalizationPerUser2 = new List<aspnet_PersonalizationPerUser>();
            this.aspnet_UsersInRoles = new List<aspnet_UsersInRoles>();
            this.aspnet_UsersInRoles1 = new List<aspnet_UsersInRoles>();
            this.aspnet_UsersInRoles2 = new List<aspnet_UsersInRoles>();
            this.DebitCardOrders = new List<DebitCardOrder>();
            this.PINDebitOrders = new List<PINDebitOrder>();
            this.PrepaidCollectOrders = new List<PrepaidCollectOrder>();
            this.TrustDepositOrders = new List<TrustDepositOrder>();
            this.User_Facility = new List<User_Facility>();
            this.CustomerAccounts = new List<CustomerAccount>();
        }

        public System.Guid ApplicationId { get; set; }
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public string LoweredUserName { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public System.DateTime LastActivityDate { get; set; }
        public virtual aspnet_Applications aspnet_Applications { get; set; }
        public virtual aspnet_Applications aspnet_Applications1 { get; set; }
        public virtual aspnet_Applications aspnet_Applications2 { get; set; }
        public virtual aspnet_Membership aspnet_Membership { get; set; }
        public virtual aspnet_Membership aspnet_Membership1 { get; set; }
        public virtual aspnet_Membership aspnet_Membership2 { get; set; }
        public virtual ICollection<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public virtual ICollection<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser1 { get; set; }
        public virtual ICollection<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser2 { get; set; }
        public virtual aspnet_Profile aspnet_Profile { get; set; }
        public virtual aspnet_Profile aspnet_Profile1 { get; set; }
        public virtual aspnet_Profile aspnet_Profile2 { get; set; }
        public virtual ICollection<aspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }
        public virtual ICollection<aspnet_UsersInRoles> aspnet_UsersInRoles1 { get; set; }
        public virtual ICollection<aspnet_UsersInRoles> aspnet_UsersInRoles2 { get; set; }
        public virtual ICollection<DebitCardOrder> DebitCardOrders { get; set; }
        public virtual ICollection<PINDebitOrder> PINDebitOrders { get; set; }
        public virtual ICollection<PrepaidCollectOrder> PrepaidCollectOrders { get; set; }
        public virtual ICollection<TrustDepositOrder> TrustDepositOrders { get; set; }
        public virtual ICollection<User_Facility> User_Facility { get; set; }
        public virtual ICollection<CustomerAccount> CustomerAccounts { get; set; }
    }
}
