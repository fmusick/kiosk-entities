using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class KioskMap : EntityTypeConfiguration<Kiosk>
    {
        public KioskMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.secret_key)
                .IsRequired()
                .HasMaxLength(2048);

            // Table & Column Mappings
            this.ToTable("Kiosk");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.secret_key).HasColumnName("secret_key");
            this.Property(t => t.location_id).HasColumnName("location_id");

            // Relationships
            this.HasRequired(t => t.Location)
                .WithMany(t => t.Kiosks)
                .HasForeignKey(d => d.location_id);

        }
    }
}
