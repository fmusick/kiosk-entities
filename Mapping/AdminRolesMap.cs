using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class AdminRolesMap : EntityTypeConfiguration<AdminRoles>
    {
        public AdminRolesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.RoleId });

            // Properties
            this.Property(t => t.Rolename)
                .HasMaxLength(256);

            this.Property(t => t.ApplicationName)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("AdminRoles");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.Rolename).HasColumnName("Rolename");
            this.Property(t => t.ApplicationName).HasColumnName("ApplicationName");
        }
    }
}
