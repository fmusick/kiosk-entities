using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DebitCardTransactionMap : EntityTypeConfiguration<DebitCardTransaction>
    {
        public DebitCardTransactionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.Type, t.CardId, t.CardValue });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PurchaseMethod)
                .HasMaxLength(255);

            this.Property(t => t.TransactionId)
                .HasMaxLength(50);

            this.Property(t => t.CreditAuthorization)
                .HasMaxLength(255);

            this.Property(t => t.CreditTransactionId)
                .HasMaxLength(255);

            this.Property(t => t.Inmate)
                .HasMaxLength(255);

            this.Property(t => t.UserName)
                .HasMaxLength(256);

            this.Property(t => t.Outlet)
                .HasMaxLength(5);

            this.Property(t => t.CallerName)
                .HasMaxLength(102);

            this.Property(t => t.CallerEmail)
                .HasMaxLength(100);

            this.Property(t => t.CallerPhone)
                .HasMaxLength(20);

            this.Property(t => t.CardId)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.CardValue)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("DebitCardTransactions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.PurchasedOn).HasColumnName("PurchasedOn");
            this.Property(t => t.PurchaseMethod).HasColumnName("PurchaseMethod");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.BillCount).HasColumnName("BillCount");
            this.Property(t => t.HandlingFee).HasColumnName("HandlingFee");
            this.Property(t => t.Surcharge).HasColumnName("Surcharge");
            this.Property(t => t.CreditAuthorization).HasColumnName("CreditAuthorization");
            this.Property(t => t.CreditTransactionDate).HasColumnName("CreditTransactionDate");
            this.Property(t => t.CreditTransactionId).HasColumnName("CreditTransactionId");
            this.Property(t => t.CreditTransactionAmount).HasColumnName("CreditTransactionAmount");
            this.Property(t => t.CreditTransactionFee).HasColumnName("CreditTransactionFee");
            this.Property(t => t.Inmate).HasColumnName("Inmate");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Outlet).HasColumnName("Outlet");
            this.Property(t => t.CallerName).HasColumnName("CallerName");
            this.Property(t => t.CallerEmail).HasColumnName("CallerEmail");
            this.Property(t => t.CallerPhone).HasColumnName("CallerPhone");
            this.Property(t => t.CardId).HasColumnName("CardId");
            this.Property(t => t.CardValue).HasColumnName("CardValue");
        }
    }
}
