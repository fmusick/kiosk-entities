using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class TrustDepositTransactionMap : EntityTypeConfiguration<TrustDepositTransaction>
    {
        public TrustDepositTransactionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.Type, t.Amount, t.DepositedOn, t.PurchaseMethod, t.TransactionId, t.HandlingFee, t.Surcharge });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PurchaseMethod)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.TransactionId)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.HandlingFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Surcharge)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CreditAuthorization)
                .HasMaxLength(255);

            this.Property(t => t.CreditTransactionId)
                .HasMaxLength(255);

            this.Property(t => t.Inmate)
                .HasMaxLength(512);

            this.Property(t => t.UserName)
                .HasMaxLength(256);

            this.Property(t => t.Outlet)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("TrustDepositTransactions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DepositedOn).HasColumnName("DepositedOn");
            this.Property(t => t.PurchaseMethod).HasColumnName("PurchaseMethod");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.BillCount).HasColumnName("BillCount");
            this.Property(t => t.HandlingFee).HasColumnName("HandlingFee");
            this.Property(t => t.Surcharge).HasColumnName("Surcharge");
            this.Property(t => t.CreditAuthorization).HasColumnName("CreditAuthorization");
            this.Property(t => t.CreditTransactionDate).HasColumnName("CreditTransactionDate");
            this.Property(t => t.CreditTransactionId).HasColumnName("CreditTransactionId");
            this.Property(t => t.CreditTransactionAmount).HasColumnName("CreditTransactionAmount");
            this.Property(t => t.CreditTransactionFee).HasColumnName("CreditTransactionFee");
            this.Property(t => t.Inmate).HasColumnName("Inmate");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Outlet).HasColumnName("Outlet");
        }
    }
}
