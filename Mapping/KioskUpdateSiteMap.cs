using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class KioskUpdateSiteMap : EntityTypeConfiguration<KioskUpdateSite>
    {
        public KioskUpdateSiteMap()
        {
            // Primary Key
            this.HasKey(t => new { t.rec_id, t.site_id, t.dataroot, t.fileroot, t.templife, t.docid_is_key, t.kiosk_delete, t.warn_limit, t.warn_window, t.file_wait, t.append_site_to_ui });

            // Properties
            this.Property(t => t.rec_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.site_id)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.user)
                .IsFixedLength()
                .HasMaxLength(20);

            this.Property(t => t.site_name)
                .IsFixedLength()
                .HasMaxLength(32);

            this.Property(t => t.dataroot)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.fileroot)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.filemask)
                .IsFixedLength()
                .HasMaxLength(24);

            this.Property(t => t.fileext)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.templife)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.root_tag)
                .HasMaxLength(50);

            this.Property(t => t.record_tag)
                .HasMaxLength(50);

            this.Property(t => t.pin_tag)
                .HasMaxLength(50);

            this.Property(t => t.docid_tag)
                .HasMaxLength(50);

            this.Property(t => t.fname_tag)
                .HasMaxLength(50);

            this.Property(t => t.mname_tag)
                .HasMaxLength(50);

            this.Property(t => t.lname_tag)
                .HasMaxLength(50);

            this.Property(t => t.dob_tag)
                .HasMaxLength(50);

            this.Property(t => t.ui_tag)
                .HasMaxLength(50);

            this.Property(t => t.lname_fname_tag)
                .HasMaxLength(50);

            this.Property(t => t.fname_lname_tag)
                .HasMaxLength(50);

            this.Property(t => t.action_tag)
                .HasMaxLength(50);

            this.Property(t => t.action_attribute)
                .HasMaxLength(50);

            this.Property(t => t.add_tag)
                .HasMaxLength(50);

            this.Property(t => t.add_text)
                .HasMaxLength(50);

            this.Property(t => t.remove_tag)
                .HasMaxLength(50);

            this.Property(t => t.remove_text)
                .HasMaxLength(50);

            this.Property(t => t.balance_tag)
                .HasMaxLength(50);

            this.Property(t => t.site_tag)
                .HasMaxLength(50);

            this.Property(t => t.kiosk_server)
                .HasMaxLength(255);

            this.Property(t => t.kiosk_sqluid)
                .HasMaxLength(50);

            this.Property(t => t.kiosk_sqlpswd)
                .HasMaxLength(50);

            this.Property(t => t.kiosk_db)
                .HasMaxLength(50);

            this.Property(t => t.last_report)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.report_time)
                .IsFixedLength()
                .HasMaxLength(4);

            this.Property(t => t.email_to)
                .HasMaxLength(512);

            this.Property(t => t.email_cc)
                .HasMaxLength(512);

            this.Property(t => t.alert_email)
                .HasMaxLength(512);

            this.Property(t => t.warn_limit)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.warn_window)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.file_wait)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.location_tag)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("KioskUpdateSites");
            this.Property(t => t.rec_id).HasColumnName("rec_id");
            this.Property(t => t.site_id).HasColumnName("site_id");
            this.Property(t => t.user).HasColumnName("user");
            this.Property(t => t.site_name).HasColumnName("site_name");
            this.Property(t => t.dataroot).HasColumnName("dataroot");
            this.Property(t => t.fileroot).HasColumnName("fileroot");
            this.Property(t => t.filemask).HasColumnName("filemask");
            this.Property(t => t.fileext).HasColumnName("fileext");
            this.Property(t => t.templife).HasColumnName("templife");
            this.Property(t => t.docid_is_key).HasColumnName("docid_is_key");
            this.Property(t => t.root_tag).HasColumnName("root_tag");
            this.Property(t => t.record_tag).HasColumnName("record_tag");
            this.Property(t => t.pin_tag).HasColumnName("pin_tag");
            this.Property(t => t.docid_tag).HasColumnName("docid_tag");
            this.Property(t => t.fname_tag).HasColumnName("fname_tag");
            this.Property(t => t.mname_tag).HasColumnName("mname_tag");
            this.Property(t => t.lname_tag).HasColumnName("lname_tag");
            this.Property(t => t.dob_tag).HasColumnName("dob_tag");
            this.Property(t => t.ui_tag).HasColumnName("ui_tag");
            this.Property(t => t.lname_fname_tag).HasColumnName("lname_fname_tag");
            this.Property(t => t.fname_lname_tag).HasColumnName("fname_lname_tag");
            this.Property(t => t.action_tag).HasColumnName("action_tag");
            this.Property(t => t.action_attribute).HasColumnName("action_attribute");
            this.Property(t => t.add_tag).HasColumnName("add_tag");
            this.Property(t => t.add_text).HasColumnName("add_text");
            this.Property(t => t.remove_tag).HasColumnName("remove_tag");
            this.Property(t => t.remove_text).HasColumnName("remove_text");
            this.Property(t => t.balance_tag).HasColumnName("balance_tag");
            this.Property(t => t.site_tag).HasColumnName("site_tag");
            this.Property(t => t.balance_adjust).HasColumnName("balance_adjust");
            this.Property(t => t.kiosk_facility_id).HasColumnName("kiosk_facility_id");
            this.Property(t => t.kiosk_server).HasColumnName("kiosk_server");
            this.Property(t => t.kiosk_sqluid).HasColumnName("kiosk_sqluid");
            this.Property(t => t.kiosk_sqlpswd).HasColumnName("kiosk_sqlpswd");
            this.Property(t => t.kiosk_db).HasColumnName("kiosk_db");
            this.Property(t => t.kiosk_delete).HasColumnName("kiosk_delete");
            this.Property(t => t.last_report).HasColumnName("last_report");
            this.Property(t => t.report_time).HasColumnName("report_time");
            this.Property(t => t.email_to).HasColumnName("email_to");
            this.Property(t => t.email_cc).HasColumnName("email_cc");
            this.Property(t => t.alert_email).HasColumnName("alert_email");
            this.Property(t => t.last_file).HasColumnName("last_file");
            this.Property(t => t.last_update).HasColumnName("last_update");
            this.Property(t => t.warn_limit).HasColumnName("warn_limit");
            this.Property(t => t.warn_window).HasColumnName("warn_window");
            this.Property(t => t.file_wait).HasColumnName("file_wait");
            this.Property(t => t.last_warning).HasColumnName("last_warning");
            this.Property(t => t.append_site_to_ui).HasColumnName("append_site_to_ui");
            this.Property(t => t.location_tag).HasColumnName("location_tag");
        }
    }
}
