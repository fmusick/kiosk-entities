using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DebitCardMap : EntityTypeConfiguration<DebitCard>
    {
        public DebitCardMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.cardID)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.MMbatchID)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.cardType)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            this.Property(t => t.subtitle)
                .HasMaxLength(255);

            this.Property(t => t.instructions)
                .HasMaxLength(255);

            this.Property(t => t.disclaimer)
                .HasMaxLength(255);

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("DebitCard");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.batchID).HasColumnName("batchID");
            this.Property(t => t.cardID).HasColumnName("cardID");
            this.Property(t => t.facilityID).HasColumnName("facilityID");
            this.Property(t => t.MMbatchID).HasColumnName("MMbatchID");
            this.Property(t => t.seqNum).HasColumnName("seqNum");
            this.Property(t => t.cardType).HasColumnName("cardType");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.subtitle).HasColumnName("subtitle");
            this.Property(t => t.instructions).HasColumnName("instructions");
            this.Property(t => t.disclaimer).HasColumnName("disclaimer");
            this.Property(t => t.enabled).HasColumnName("enabled");
            this.Property(t => t.activationDate).HasColumnName("activationDate");
            this.Property(t => t.expirationDate).HasColumnName("expirationDate");
            this.Property(t => t.expInXDays).HasColumnName("expInXDays");
            this.Property(t => t.minAmt).HasColumnName("minAmt");
            this.Property(t => t.centValue).HasColumnName("centValue");
            this.Property(t => t.callsAllowed).HasColumnName("callsAllowed");
            this.Property(t => t.units).HasColumnName("units");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.value).HasColumnName("value");
            this.Property(t => t.imported_on).HasColumnName("imported_on");
            this.Property(t => t.kioskID).HasColumnName("kioskID");
        }
    }
}
