using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.product_catalogue_id);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.description)
                .HasMaxLength(500);

            this.Property(t => t.value)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Products");
            this.Property(t => t.id).HasColumnName("Id");
            this.Property(t => t.product_catalogue_id).HasColumnName("ProductCatalog_Id");
            this.Property(t => t.name).HasColumnName("Name");
            this.Property(t => t.description).HasColumnName("Description");
            this.Property(t => t.value).HasColumnName("Value");
        }
    }
}
