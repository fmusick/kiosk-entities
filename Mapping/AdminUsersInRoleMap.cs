using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class AdminUsersInRoleMap : EntityTypeConfiguration<AdminUsersInRole>
    {
        public AdminUsersInRoleMap()
        {
            // Primary Key
            this.HasKey(t => new { t.RoleId, t.UserId });

            // Properties
            this.Property(t => t.Username)
                .HasMaxLength(256);

            this.Property(t => t.Rolename)
                .HasMaxLength(256);

            this.Property(t => t.ApplicationName)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("AdminUsersInRoles");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Username).HasColumnName("Username");
            this.Property(t => t.Rolename).HasColumnName("Rolename");
            this.Property(t => t.ApplicationName).HasColumnName("ApplicationName");
        }
    }
}
