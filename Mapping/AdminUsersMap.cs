using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class AdminUsersMap : EntityTypeConfiguration<AdminUsers>
    {
        public AdminUsersMap()
        {
            // Primary Key
            this.HasKey(t => t.UserId);

            // Properties
            this.Property(t => t.Username)
                .HasMaxLength(256);

            this.Property(t => t.Firstname)
                .HasMaxLength(256);

            this.Property(t => t.Lastname)
                .HasMaxLength(256);

            this.Property(t => t.Email)
                .HasMaxLength(256);

            this.Property(t => t.Password)
                .HasMaxLength(256);

            this.Property(t => t.LastActivityDate);

            this.Property(t => t.CreationDate);

            // Table & Column Mappings
            this.ToTable("AdminUsers");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Username).HasColumnName("Username");
            this.Property(t => t.Firstname).HasColumnName("Firstname");
            this.Property(t => t.Lastname).HasColumnName("Lastname");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.LastActivityDate).HasColumnName("LastActivityDate");
            this.Property(t => t.CreationDate).HasColumnName("CreationDate");
        }
    }
}
