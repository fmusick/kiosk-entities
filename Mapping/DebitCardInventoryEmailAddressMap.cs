using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DebitCardInventoryEmailAddressMap : EntityTypeConfiguration<DebitCardInventoryEmailAddress>
    {
        public DebitCardInventoryEmailAddressMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.email)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("DebitCardInventoryEmailAddress");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.email).HasColumnName("email");
        }
    }
}
