using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class aspnet_UsersInRolesMap : EntityTypeConfiguration<aspnet_UsersInRoles>
    {
        public aspnet_UsersInRolesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserId, t.RoleId });

            // Properties
            // Table & Column Mappings
            this.ToTable("aspnet_UsersInRoles");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");

            // Relationships
            this.HasRequired(t => t.aspnet_Roles)
                .WithMany(t => t.aspnet_UsersInRoles)
                .HasForeignKey(d => d.RoleId);
            this.HasRequired(t => t.aspnet_Roles1)
                .WithMany(t => t.aspnet_UsersInRoles1)
                .HasForeignKey(d => d.RoleId);
            this.HasRequired(t => t.aspnet_Roles2)
                .WithMany(t => t.aspnet_UsersInRoles2)
                .HasForeignKey(d => d.RoleId);
            this.HasRequired(t => t.aspnet_Users)
                .WithMany(t => t.aspnet_UsersInRoles)
                .HasForeignKey(d => d.UserId);
            this.HasRequired(t => t.aspnet_Users1)
                .WithMany(t => t.aspnet_UsersInRoles1)
                .HasForeignKey(d => d.UserId);
            this.HasRequired(t => t.aspnet_Users2)
                .WithMany(t => t.aspnet_UsersInRoles2)
                .HasForeignKey(d => d.UserId);

        }
    }
}
