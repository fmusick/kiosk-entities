using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DebitCardOrderMap : EntityTypeConfiguration<DebitCardOrder>
    {
        public DebitCardOrderMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CallerFirstName)
                .HasMaxLength(50);

            this.Property(t => t.CallerLastName)
                .HasMaxLength(50);

            this.Property(t => t.CallerEmail)
                .HasMaxLength(100);

            this.Property(t => t.CallerPhone)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("DebitCardOrders");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.aspnet_Users_UserId).HasColumnName("aspnet_Users_UserId");
            this.Property(t => t.DebitCardPurchase_id).HasColumnName("DebitCardPurchase_id");
            this.Property(t => t.CallerFirstName).HasColumnName("CallerFirstName");
            this.Property(t => t.CallerLastName).HasColumnName("CallerLastName");
            this.Property(t => t.CallerEmail).HasColumnName("CallerEmail");
            this.Property(t => t.CallerPhone).HasColumnName("CallerPhone");

            // Relationships
            this.HasOptional(t => t.aspnet_Users)
                .WithMany(t => t.DebitCardOrders)
                .HasForeignKey(d => d.aspnet_Users_UserId);

            this.HasOptional(t => t.DebitCardPurchase)
                .WithMany(t => t.DebitCardOrders)
                .HasForeignKey(d => d.DebitCardPurchase_id);

        }
    }
}
