using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class BailDepositMap : EntityTypeConfiguration<BailDeposit>
    {
        public BailDepositMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.customer_photo)
                .HasMaxLength(8000);

            this.Property(t => t.purchase_method)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.trans_ID)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.terminate_text)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("BailDeposit");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.amount).HasColumnName("amount");
            this.Property(t => t.deposited_on).HasColumnName("deposited_on");
            this.Property(t => t.customer_photo).HasColumnName("customer_photo");
            this.Property(t => t.purchase_method).HasColumnName("purchase_method");
            this.Property(t => t.trans_ID).HasColumnName("trans_ID");
            this.Property(t => t.bill_cnt).HasColumnName("bill_cnt");
            this.Property(t => t.handling_fee).HasColumnName("handling_fee");
            this.Property(t => t.surcharge).HasColumnName("surcharge");
            this.Property(t => t.terminate_code).HasColumnName("terminate_code");
            this.Property(t => t.terminate_text).HasColumnName("terminate_text");
            this.Property(t => t.refund_state).HasColumnName("refund_state");
            this.Property(t => t.inmatebail_id).HasColumnName("inmatebail_id");
            this.Property(t => t.credit_authorization_id).HasColumnName("credit_authorization_id");
            this.Property(t => t.facility_id).HasColumnName("facility_id");
            this.Property(t => t.purchased_from_id).HasColumnName("purchased_from_id");

            // Relationships
            this.HasOptional(t => t.Kiosk)
                .WithMany(t => t.BailDeposits)
                .HasForeignKey(d => d.purchased_from_id);

            //this.HasRequired(t => t.Facility)
            //    .WithMany(t => t.BailDeposits)
            //    .HasForeignKey(d => d.facility_id);

            this.HasRequired(t => t.InmateBail)
                .WithMany(t => t.BailDeposits)
                .HasForeignKey(d => d.inmatebail_id);

            this.HasOptional(t => t.CreditCardTransaction)
                .WithMany(t => t.BailDeposits)
                .HasForeignKey(d => d.credit_authorization_id);

        }
    }
}
