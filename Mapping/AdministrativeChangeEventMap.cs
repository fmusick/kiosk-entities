using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class AdministrativeChangeEventMap : EntityTypeConfiguration<AdministrativeChangeEvent>
    {
        public AdministrativeChangeEventMap()
        {
            // Primary Key
            this.HasKey(t => t.BaseEvent_id);

            // Properties
            this.Property(t => t.BaseEvent_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.section)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.username)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("AdministrativeChangeEvent");
            this.Property(t => t.BaseEvent_id).HasColumnName("BaseEvent_id");
            this.Property(t => t.section).HasColumnName("section");
            this.Property(t => t.username).HasColumnName("username");

            // Relationships
            this.HasRequired(t => t.BaseEvent)
                .WithOptional(t => t.AdministrativeChangeEvent);

        }
    }
}
