using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class SoftwareVersionMap : EntityTypeConfiguration<SoftwareVersion>
    {
        public SoftwareVersionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.assembly_version)
                .HasMaxLength(255);

            this.Property(t => t.download_url)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("SoftwareVersion");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.released_on).HasColumnName("released_on");
            this.Property(t => t.assembly_version).HasColumnName("assembly_version");
            this.Property(t => t.download_url).HasColumnName("download_url");
        }
    }
}
