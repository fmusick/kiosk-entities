using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class FacilityProductMap : EntityTypeConfiguration<FacilityProduct>
    {
        public FacilityProductMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.facility_id)
                .IsRequired();

            this.Property(t => t.product_id)
                .IsRequired();

            this.Property(t => t.enabled)
                .IsRequired();

            this.Property(t => t.allow_inmate_search)
                .IsRequired();

            this.Property(t => t.amount)
                .IsRequired();

            this.Property(t => t.quantity_maximum)
                .IsRequired();

            this.Property(t => t.quantity_minimum)
                .IsRequired();

            this.Property(t => t.report_email)
                .HasMaxLength(100);

            this.Property(t => t.receipt_email)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("FacilityProducts");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.facility_id).HasColumnName("Facility_Id");
            this.Property(t => t.product_id).HasColumnName("Product_Id");
            this.Property(t => t.enabled).HasColumnName("IsEnabled");
            this.Property(t => t.allow_inmate_search).HasColumnName("AllowInmateSearch");
            this.Property(t => t.amount).HasColumnName("Amount");
            this.Property(t => t.quantity_maximum).HasColumnName("QuantityMaximum");
            this.Property(t => t.quantity_minimum).HasColumnName("QuantityMinimum");
            this.Property(t => t.report_email).HasColumnName("ReportEmail");
            this.Property(t => t.report_time).HasColumnName("ReportTime");
            this.Property(t => t.receipt_email).HasColumnName("ReceiptEmail");
            this.Property(t => t.outlet_id).HasColumnName("Outlet_Id");
            this.Property(t => t.payment_gateway_type_id).HasColumnName("PaymentGatewayType_Id");

            // Relationships
            //this.HasRequired(t => t.Facility)
            //    .WithMany(t => t.FacilityProducts)
            //    .HasForeignKey(d => d.facility_id);

            this.HasRequired(t => t.Product)
                .WithMany(t => t.FacilityProducts)
                .HasForeignKey(d => d.product_id);
        }
    }
}
