using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class PaymentTypeMap : EntityTypeConfiguration<PaymentType>
    {
        public PaymentTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.description)
                .HasMaxLength(500);

            // Properties
            // Table & Column Mappings
            this.ToTable("PaymentTypes");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("Name");
            this.Property(t => t.description).HasColumnName("Description");
        }
    }
}
