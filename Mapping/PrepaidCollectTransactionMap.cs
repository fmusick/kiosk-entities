using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class PrepaidCollectTransactionMap : EntityTypeConfiguration<PrepaidCollectTransaction>
    {
        public PrepaidCollectTransactionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.Type, t.amount, t.DepositedOn, t.PurchaseMethod, t.TransactionId, t.HandlingFee, t.surcharge, t.Phone });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PurchaseMethod)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.TransactionId)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.HandlingFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.surcharge)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CreditAuthorization)
                .HasMaxLength(255);

            this.Property(t => t.CreditTransactionId)
                .HasMaxLength(255);

            this.Property(t => t.UserName)
                .HasMaxLength(256);

            this.Property(t => t.Outlet)
                .HasMaxLength(5);

            this.Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.CallerName)
                .HasMaxLength(102);

            this.Property(t => t.CallerEmail)
                .HasMaxLength(100);

            this.Property(t => t.CallerPhone)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("PrepaidCollectTransactions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.amount).HasColumnName("amount");
            this.Property(t => t.DepositedOn).HasColumnName("DepositedOn");
            this.Property(t => t.PurchaseMethod).HasColumnName("PurchaseMethod");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.BillCount).HasColumnName("BillCount");
            this.Property(t => t.HandlingFee).HasColumnName("HandlingFee");
            this.Property(t => t.surcharge).HasColumnName("surcharge");
            this.Property(t => t.CreditAuthorization).HasColumnName("CreditAuthorization");
            this.Property(t => t.CreditTransactionDate).HasColumnName("CreditTransactionDate");
            this.Property(t => t.CreditTransactionId).HasColumnName("CreditTransactionId");
            this.Property(t => t.CreditTransactionAmount).HasColumnName("CreditTransactionAmount");
            this.Property(t => t.CreditTransactionFee).HasColumnName("CreditTransactionFee");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Outlet).HasColumnName("Outlet");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.CallerName).HasColumnName("CallerName");
            this.Property(t => t.CallerEmail).HasColumnName("CallerEmail");
            this.Property(t => t.CallerPhone).HasColumnName("CallerPhone");
        }
    }
}
