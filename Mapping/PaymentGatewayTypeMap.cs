using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class PaymentGatewayTypeMap : EntityTypeConfiguration<PaymentGatewayType>
    {
        public PaymentGatewayTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.name_space)
                .IsRequired();

            // Properties
            // Table & Column Mappings
            this.ToTable("PaymentGatewayType");
            this.Property(t => t.id).HasColumnName("Id");
            this.Property(t => t.name).HasColumnName("Name");
            this.Property(t => t.name_space).HasColumnName("Namespace");
            this.Property(t => t.gateway_type).HasColumnName("gatewayType");
            this.Property(t => t.api_login_id).HasColumnName("APILoginId");
            this.Property(t => t.transaction_key).HasColumnName("TransactionKey");
            this.Property(t => t.in_test_mode).HasColumnName("InTestMode");
            this.Property(t => t.username).HasColumnName("Username");
            this.Property(t => t.password).HasColumnName("Password");
            this.Property(t => t.processor_id).HasColumnName("ProcessorId");
            this.Property(t => t.api_key).HasColumnName("APIKey");
            this.Property(t => t.url).HasColumnName("Url");
        }
    }
}
