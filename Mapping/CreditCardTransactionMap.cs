using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class CreditCardTransactionMap : EntityTypeConfiguration<CreditCardTransaction>
    {
        public CreditCardTransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.authorization_code)
                .HasMaxLength(255);

            this.Property(t => t.transaction_id)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("CreditCardTransaction");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.authorization_code).HasColumnName("authorization_code");
            this.Property(t => t.transaction_datetime).HasColumnName("transaction_datetime");
            this.Property(t => t.transaction_id).HasColumnName("transaction_id");
            this.Property(t => t.transaction_amount).HasColumnName("transaction_amount");
            this.Property(t => t.transaction_fee).HasColumnName("transaction_fee");
        }
    }
}
