using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class PrepaidCollectOrderMap : EntityTypeConfiguration<PrepaidCollectOrder>
    {
        public PrepaidCollectOrderMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.CallerFirstName)
                .HasMaxLength(50);

            this.Property(t => t.CallerLastName)
                .HasMaxLength(50);

            this.Property(t => t.CallerEmail)
                .HasMaxLength(100);

            this.Property(t => t.CallerPhone)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("PrepaidCollectOrders");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.aspnet_Users_UserId).HasColumnName("aspnet_Users_UserId");
            this.Property(t => t.PrepaidCollectDepost_id).HasColumnName("PrepaidCollectDepost_id");
            this.Property(t => t.CallerFirstName).HasColumnName("CallerFirstName");
            this.Property(t => t.CallerLastName).HasColumnName("CallerLastName");
            this.Property(t => t.CallerEmail).HasColumnName("CallerEmail");
            this.Property(t => t.CallerPhone).HasColumnName("CallerPhone");

            // Relationships
            this.HasOptional(t => t.aspnet_Users)
                .WithMany(t => t.PrepaidCollectOrders)
                .HasForeignKey(d => d.aspnet_Users_UserId);

            this.HasOptional(t => t.PrepaidCollectDeposit)
                .WithMany(t => t.PrepaidCollectOrders)
                .HasForeignKey(d => d.PrepaidCollectDepost_id);

        }
    }
}
