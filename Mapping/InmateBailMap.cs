using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class InmateBailMap : EntityTypeConfiguration<InmateBail>
    {
        public InmateBailMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("InmateBail");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.bail_amount).HasColumnName("bail_amount");
            this.Property(t => t.fee).HasColumnName("fee");
            this.Property(t => t.incarceration_id).HasColumnName("incarceration_id");

            // Relationships
            this.HasRequired(t => t.Incarceration)
                .WithMany(t => t.InmateBails)
                .HasForeignKey(d => d.incarceration_id);

        }
    }
}
