using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class FacilityInmateEventMap : EntityTypeConfiguration<FacilityInmateEvent>
    {
        public FacilityInmateEventMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.activity)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("FacilityInmateEvent");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.activity).HasColumnName("activity");
            this.Property(t => t.inmate_id).HasColumnName("inmate_id");
            this.Property(t => t.facility_id).HasColumnName("facility_id");

            // Relationships
            //this.HasRequired(t => t.Facility)
            //    .WithMany(t => t.FacilityInmateEvents)
            //    .HasForeignKey(d => d.facility_id);

        }
    }
}
