using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class BaseEventMap : EntityTypeConfiguration<BaseEvent>
    {
        public BaseEventMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.title)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.message)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("BaseEvent");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.message).HasColumnName("message");
            this.Property(t => t.occurred).HasColumnName("occurred");
        }
    }
}
