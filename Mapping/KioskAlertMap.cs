using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class KioskAlertMap : EntityTypeConfiguration<KioskAlert>
    {
        public KioskAlertMap()
        {
            // Primary Key
            this.HasKey(t => t.BaseEvent_id);

            // Properties
            this.Property(t => t.BaseEvent_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.koisk_id)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("KioskAlert");
            this.Property(t => t.BaseEvent_id).HasColumnName("BaseEvent_id");
            this.Property(t => t.koisk_id).HasColumnName("kiosk_id");

            // Relationships
            this.HasRequired(t => t.BaseEvent)
                .WithOptional(t => t.KioskAlert);
        }
    }
}
