using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class FF2XMLSitesMap : EntityTypeConfiguration<FF2XMLSites>
    {
        public FF2XMLSitesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.site_id, t.fileroot, t.templife, t.name_max_length, t.field_count, t.seperator, t.warn_limit, t.warn_window, t.dataroot });

            // Properties
            this.Property(t => t.site_id)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.user)
                .IsFixedLength()
                .HasMaxLength(20);

            this.Property(t => t.site_name)
                .IsFixedLength()
                .HasMaxLength(32);

            this.Property(t => t.fileroot)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.fileext)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.templife)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.name_max_length)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.field_count)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.seperator)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.ftpsite)
                .IsFixedLength()
                .HasMaxLength(30);

            this.Property(t => t.ftpuid)
                .IsFixedLength()
                .HasMaxLength(20);

            this.Property(t => t.ftppswd)
                .IsFixedLength()
                .HasMaxLength(20);

            this.Property(t => t.last_report)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.report_time)
                .IsFixedLength()
                .HasMaxLength(4);

            this.Property(t => t.email_to)
                .HasMaxLength(512);

            this.Property(t => t.email_cc)
                .HasMaxLength(512);

            this.Property(t => t.alert_email)
                .HasMaxLength(512);

            this.Property(t => t.warn_limit)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.warn_window)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.dataroot)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.xmlhead)
                .HasMaxLength(1024);

            this.Property(t => t.xmltail)
                .HasMaxLength(1024);

            // Table & Column Mappings
            this.ToTable("FF2XMLSites");
            this.Property(t => t.site_id).HasColumnName("site_id");
            this.Property(t => t.user).HasColumnName("user");
            this.Property(t => t.site_name).HasColumnName("site_name");
            this.Property(t => t.fileroot).HasColumnName("fileroot");
            this.Property(t => t.fileext).HasColumnName("fileext");
            this.Property(t => t.templife).HasColumnName("templife");
            this.Property(t => t.name_max_length).HasColumnName("name_max_length");
            this.Property(t => t.field_count).HasColumnName("field_count");
            this.Property(t => t.seperator).HasColumnName("seperator");
            this.Property(t => t.ftpsite).HasColumnName("ftpsite");
            this.Property(t => t.ftpuid).HasColumnName("ftpuid");
            this.Property(t => t.ftppswd).HasColumnName("ftppswd");
            this.Property(t => t.ftpwait).HasColumnName("ftpwait");
            this.Property(t => t.last_report).HasColumnName("last_report");
            this.Property(t => t.report_time).HasColumnName("report_time");
            this.Property(t => t.email_to).HasColumnName("email_to");
            this.Property(t => t.email_cc).HasColumnName("email_cc");
            this.Property(t => t.alert_email).HasColumnName("alert_email");
            this.Property(t => t.last_file).HasColumnName("last_file");
            this.Property(t => t.last_update).HasColumnName("last_update");
            this.Property(t => t.warn_limit).HasColumnName("warn_limit");
            this.Property(t => t.warn_window).HasColumnName("warn_window");
            this.Property(t => t.last_warning).HasColumnName("last_warning");
            this.Property(t => t.ftpretrys).HasColumnName("ftpretrys");
            this.Property(t => t.dataroot).HasColumnName("dataroot");
            this.Property(t => t.xmlhead).HasColumnName("xmlhead");
            this.Property(t => t.xmltail).HasColumnName("xmltail");
            this.Property(t => t.xmlparse).HasColumnName("xmlparse");
            this.Property(t => t.disc_header_rows).HasColumnName("disc_header_rows");
            this.Property(t => t.disc_footer_rows).HasColumnName("disc_footer_rows");
        }
    }
}
