using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class FacilityProductFeeMap : EntityTypeConfiguration<FacilityProductFee>
    {
        public FacilityProductFeeMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            this.Property(t => t.value)
                .IsRequired();

            // Properties
            // Table & Column Mappings
            this.ToTable("FacilityProductFees");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.facility_product_id).HasColumnName("FacilityProduct_Id");
            this.Property(t => t.fee_type_id).HasColumnName("FeeType_Id");
            this.Property(t => t.payment_type_id).HasColumnName("PaymentType_Id");
            this.Property(t => t.value).HasColumnName("Value");

            // Relationships
            this.HasRequired(t => t.FacilityProduct)
                .WithMany(t => t.FacilityProductFees)
                .HasForeignKey(d => d.facility_product_id);

            this.HasRequired(t => t.FeeType)
                .WithMany(t => t.FacilityProductFees)
                .HasForeignKey(d => d.fee_type_id);

            this.HasRequired(t => t.PaymentType)
                .WithMany(t => t.FacilityProductFees)
                .HasForeignKey(d => d.payment_type_id);

        }
    }
}
