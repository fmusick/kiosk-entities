using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DebitCardBatchMap : EntityTypeConfiguration<DebitCardBatch>
    {
        public DebitCardBatchMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.MMbatchID)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.cardType)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            this.Property(t => t.subtitle)
                .HasMaxLength(255);

            this.Property(t => t.instructions)
                .HasMaxLength(255);

            this.Property(t => t.disclaimer)
                .HasMaxLength(255);

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("DebitCardBatch");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.created).HasColumnName("created");
            this.Property(t => t.MMbatchID).HasColumnName("MMbatchID");
            this.Property(t => t.cardType).HasColumnName("cardType");
            this.Property(t => t.seqMin).HasColumnName("seqMin");
            this.Property(t => t.seqMax).HasColumnName("seqMax");
            this.Property(t => t.cardCount).HasColumnName("cardCount");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.subtitle).HasColumnName("subtitle");
            this.Property(t => t.instructions).HasColumnName("instructions");
            this.Property(t => t.disclaimer).HasColumnName("disclaimer");
            this.Property(t => t.enabled).HasColumnName("enabled");
            this.Property(t => t.activationDate).HasColumnName("activationDate");
            this.Property(t => t.expirationDate).HasColumnName("expirationDate");
            this.Property(t => t.expInXDays).HasColumnName("expInXDays");
            this.Property(t => t.minuteAmt).HasColumnName("minuteAmt");
            this.Property(t => t.dollarAmount).HasColumnName("dollarAmount");
            this.Property(t => t.callsAllowed).HasColumnName("callsAllowed");
            this.Property(t => t.units).HasColumnName("units");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.facility_id).HasColumnName("facility_id");

            // Relationships
            //this.HasRequired(t => t.Facility)
            //    .WithMany(t => t.DebitCardBatches)
            //    .HasForeignKey(d => d.facility_id);

        }
    }
}
