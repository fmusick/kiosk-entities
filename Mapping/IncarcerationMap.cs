using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class IncarcerationMap : EntityTypeConfiguration<Incarceration>
    {
        public IncarcerationMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.inmate_identifier)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Incarceration");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.inmate_identifier).HasColumnName("inmate_identifier");
            this.Property(t => t.incarcerated_on).HasColumnName("incarcerated_on");
            this.Property(t => t.released_on).HasColumnName("released_on");
            this.Property(t => t.facility_id).HasColumnName("facility_id");
            this.Property(t => t.inmate_id).HasColumnName("inmate_id");

            // Relationships
            //this.HasRequired(t => t.Facility)
            //    .WithMany(t => t.Incarcerations)
            //    .HasForeignKey(d => d.facility_id);

            this.HasRequired(t => t.Inmate)
                .WithMany(t => t.Incarcerations)
                .HasForeignKey(d => d.inmate_id);

        }
    }
}
