using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class CustomerAccountMap : EntityTypeConfiguration<CustomerAccount>
    {
        public CustomerAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.first_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.last_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.address_1)
                .HasMaxLength(100);

            this.Property(t => t.address_2)
                .HasMaxLength(500);

            this.Property(t => t.city)
                .HasMaxLength(50);

            this.Property(t => t.state)
                .HasMaxLength(50);

            this.Property(t => t.country)
                .HasMaxLength(50);

            this.Property(t => t.zip)
                .HasMaxLength(10);

            this.Property(t => t.account_pin)
                .HasMaxLength(30);

            this.Property(t => t.email)
                .HasMaxLength(100);

            this.Property(t => t.phone_number)
                .HasMaxLength(20);

            this.Property(t => t.time_to_contact)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("CustomerAccount");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.first_name).HasColumnName("first_name");
            this.Property(t => t.last_name).HasColumnName("last_name");
            this.Property(t => t.address_1).HasColumnName("address_1");
            this.Property(t => t.address_2).HasColumnName("address_2");
            this.Property(t => t.city).HasColumnName("city");
            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.zip).HasColumnName("zip");
            this.Property(t => t.account_pin).HasColumnName("account_pin");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.phone_number).HasColumnName("phone_number");
            this.Property(t => t.entry_date).HasColumnName("entry_date");
            this.Property(t => t.last_activity).HasColumnName("last_activity");
            this.Property(t => t.time_to_contact).HasColumnName("time_to_contact");
        }
    }
}
