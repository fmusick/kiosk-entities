using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DirectPayPhoneNumberMap : EntityTypeConfiguration<DirectPayPhoneNumber>
    {
        public DirectPayPhoneNumberMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.phone_number)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("DirectPayPhoneNumber");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.customeraccount_id).HasColumnName("customeraccount_id");
            this.Property(t => t.phone_number).HasColumnName("phone_number");
            this.Property(t => t.description).HasColumnName("description");

            // Relationships
            this.HasRequired(t => t.CustomerAccount)
                .WithMany(t => t.DirectPayPhoneNumbers)
                .HasForeignKey(d => d.customeraccount_id);
        }
    }
}
