using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class FeeMap : EntityTypeConfiguration<Fee>
    {
        public FeeMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Fees");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.facility_id).HasColumnName("facility_id");
            this.Property(t => t.maximum_amount).HasColumnName("maximum_amount");
            this.Property(t => t.minimum_amount).HasColumnName("minimum_amount");
            this.Property(t => t.prepaidcard_percent_fee).HasColumnName("prepaidcard_percent_fee");
            this.Property(t => t.pindeposit_percent_fee).HasColumnName("pindeposit_percent_fee");
            this.Property(t => t.directpay_percent_fee).HasColumnName("directpay_percent_fee");
            this.Property(t => t.prepaidcard_flat_fee).HasColumnName("prepaidcard_flat_fee");
            this.Property(t => t.pindeposit_flat_fee).HasColumnName("pindeposit_flat_fee");
            this.Property(t => t.directpay_flat_fee).HasColumnName("directpay_flat_fee");
            this.Property(t => t.prepaidcard_flag).HasColumnName("prepaidcard_flag");
            this.Property(t => t.pindeposit_flag).HasColumnName("pindeposit_flag");
            this.Property(t => t.directpay_flag).HasColumnName("directpay_flag");
            this.Property(t => t.commissarydeposit_percent_fee).HasColumnName("commissarydeposit_percent_fee");
            this.Property(t => t.commissarydeposit_flat_fee).HasColumnName("commissarydeposit_flat_fee");
            this.Property(t => t.commissarydeposit_flag).HasColumnName("commissarydeposit_flag");
            this.Property(t => t.commissary_min).HasColumnName("commissary_min");
            this.Property(t => t.commissary_max).HasColumnName("commissary_max");
            this.Property(t => t.prepaidcard_active).HasColumnName("prepaidcard_active");
            this.Property(t => t.pindeposit_active).HasColumnName("pindeposit_active");
            this.Property(t => t.directpay_active).HasColumnName("directpay_active");
            this.Property(t => t.commissarydeposit_active).HasColumnName("commissarydeposit_active");
            this.Property(t => t.prepaidcard_max).HasColumnName("prepaidcard_max");
        }
    }
}
