using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class User_FacilityMap : EntityTypeConfiguration<User_Facility>
    {
        public User_FacilityMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("User_Facility");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.userId).HasColumnName("userId");
            this.Property(t => t.facilityId).HasColumnName("facilityId");

            // Relationships
            this.HasRequired(t => t.aspnet_Users)
                .WithMany(t => t.User_Facility)
                .HasForeignKey(d => d.userId);

            //this.HasRequired(t => t.Facility)
            //    .WithMany(t => t.User_Facility)
            //    .HasForeignKey(d => d.facilityId);

        }
    }
}
