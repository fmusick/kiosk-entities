using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class DebitCardBatchHistoryItemMap : EntityTypeConfiguration<DebitCardBatchHistoryItem>
    {
        public DebitCardBatchHistoryItemMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.batchEvent)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("DebitCardBatchHistoryItem");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.batchID).HasColumnName("batchID");
            this.Property(t => t.eventDate).HasColumnName("eventDate");
            this.Property(t => t.batchEvent).HasColumnName("batchEvent");
            this.Property(t => t.DebitCardBatch_id).HasColumnName("DebitCardBatch_id");

            // Relationships
            this.HasOptional(t => t.DebitCardBatch)
                .WithMany(t => t.DebitCardBatchHistoryItems)
                .HasForeignKey(d => d.DebitCardBatch_id);

        }
    }
}
