using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class TrustDepositOrderMap : EntityTypeConfiguration<TrustDepositOrder>
    {
        public TrustDepositOrderMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TrustDepositOrders");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.aspnet_Users_UserId).HasColumnName("aspnet_Users_UserId");
            this.Property(t => t.TrustDeposit_id).HasColumnName("TrustDeposit_id");
            this.Property(t => t.Sync).HasColumnName("Sync");

            // Relationships
            this.HasOptional(t => t.aspnet_Users)
                .WithMany(t => t.TrustDepositOrders)
                .HasForeignKey(d => d.aspnet_Users_UserId);

            this.HasOptional(t => t.TrustDeposit)
                .WithMany(t => t.TrustDepositOrders)
                .HasForeignKey(d => d.TrustDeposit_id);

        }
    }
}
