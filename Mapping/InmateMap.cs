using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Kiosk.Models.Mapping
{
    public class InmateMap : EntityTypeConfiguration<Inmate>
    {
        public InmateMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.last_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.first_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.middle_name)
                .HasMaxLength(50);

            //this.Property(t => t.unique_identifier)
            //    .HasMaxLength(255);

            //this.Property(t => t.Doc_ID)
            //    .HasMaxLength(50);

            //this.Property(t => t.Location)
            //    .HasMaxLength(100);

            this.Property(t => t.facility_id)
                .IsRequired();

            this.Property(t => t.location_id);
   

            // Table & Column Mappings
            this.ToTable("Inmate");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.last_name).HasColumnName("last_name");
            this.Property(t => t.first_name).HasColumnName("first_name");
            this.Property(t => t.middle_name).HasColumnName("middle_name");
            this.Property(t => t.created).HasColumnName("created");
            this.Property(t => t.date_of_birth).HasColumnName("date_of_birth");
            //this.Property(t => t.unique_identifier).HasColumnName("unique_identifier");
            //this.Property(t => t.commissary_balance).HasColumnName("commissary_balance");
            //this.Property(t => t.Doc_ID).HasColumnName("Doc_ID");
            //this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.location_id).HasColumnName("Location_Id");
            this.Property(t => t.facility_id).HasColumnName("facility_id");
        }
    }
}
