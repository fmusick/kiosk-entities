using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class AdministrativeChangeEvent
    {
        public int BaseEvent_id { get; set; }
        public string section { get; set; }
        public string username { get; set; }
        public virtual BaseEvent BaseEvent { get; set; }
    }
}
