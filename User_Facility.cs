using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class User_Facility
    {
        public int id { get; set; }
        public System.Guid userId { get; set; }
        public int facilityId { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }
    }
}
