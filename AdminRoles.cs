using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kiosk.Models
{
    public partial class AdminRoles
    {
        [Key]
        public System.Guid RoleId { get; set; }
        public string Rolename { get; set; }
        public string ApplicationName { get; set; }
    }
}
