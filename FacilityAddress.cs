using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class FacilityAddress
    {
        public int id { get; set; }

        public int facility_id { get; set; }

        public string street { get; set; }

        public string city { get; set; }

        public string state { get; set; }

        public string zipcode { get; set; }

        public string county { get; set; }

        public string country { get; set; }

        public string time_zone { get; set; }

        public virtual Facility Facility { get; set; }
    }
}
