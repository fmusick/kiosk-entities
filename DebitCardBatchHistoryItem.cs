using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCardBatchHistoryItem
    {
        public int id { get; set; }
        public Nullable<int> batchID { get; set; }
        public System.DateTime eventDate { get; set; }
        public string batchEvent { get; set; }
        public Nullable<int> DebitCardBatch_id { get; set; }
        public virtual DebitCardBatch DebitCardBatch { get; set; }
    }
}
