using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class aspnet_Roles
    {
        public aspnet_Roles()
        {
            this.aspnet_UsersInRoles = new List<aspnet_UsersInRoles>();
            this.aspnet_UsersInRoles1 = new List<aspnet_UsersInRoles>();
            this.aspnet_UsersInRoles2 = new List<aspnet_UsersInRoles>();
        }

        public System.Guid ApplicationId { get; set; }
        public System.Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
        public virtual aspnet_Applications aspnet_Applications { get; set; }
        public virtual aspnet_Applications aspnet_Applications1 { get; set; }
        public virtual aspnet_Applications aspnet_Applications2 { get; set; }
        public virtual ICollection<aspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }
        public virtual ICollection<aspnet_UsersInRoles> aspnet_UsersInRoles1 { get; set; }
        public virtual ICollection<aspnet_UsersInRoles> aspnet_UsersInRoles2 { get; set; }
    }
}
