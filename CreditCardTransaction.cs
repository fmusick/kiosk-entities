using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class CreditCardTransaction
    {
        public CreditCardTransaction()
        {
            this.BailDeposits = new List<BailDeposit>();
            this.PrepaidCollectDeposits = new List<PrepaidCollectDeposit>();
            this.TrustDeposits = new List<TrustDeposit>();
            this.PINDebitDeposits = new List<PINDebitDeposit>();
        }

        public int id { get; set; }
        public string authorization_code { get; set; }
        public Nullable<System.DateTime> transaction_datetime { get; set; }
        public string transaction_id { get; set; }
        public decimal transaction_amount { get; set; }
        public Nullable<decimal> transaction_fee { get; set; }
        public virtual ICollection<BailDeposit> BailDeposits { get; set; }
        public virtual ICollection<PrepaidCollectDeposit> PrepaidCollectDeposits { get; set; }
        public virtual ICollection<TrustDeposit> TrustDeposits { get; set; }
        public virtual ICollection<PINDebitDeposit> PINDebitDeposits { get; set; }
    }
}
