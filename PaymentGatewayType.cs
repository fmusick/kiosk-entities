using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kiosk.Models
{
    public partial class PaymentGatewayType
    {
        public int id { get; set; }
        public string name { get; set; }
        public string name_space { get; set; }
        public int gateway_type { get; set; }
        public string api_login_id { get; set; }
        public string transaction_key { get; set; }
        public bool in_test_mode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string processor_id { get; set; }
        public string api_key { get; set; }
        public string url { get; set; }
    }
}
