using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class aspnet_PersonalizationPerUser
    {
        public System.Guid Id { get; set; }
        public Nullable<System.Guid> PathId { get; set; }
        public Nullable<System.Guid> UserId { get; set; }
        public byte[] PageSettings { get; set; }
        public System.DateTime LastUpdatedDate { get; set; }
        public virtual aspnet_Paths aspnet_Paths { get; set; }
        public virtual aspnet_Paths aspnet_Paths1 { get; set; }
        public virtual aspnet_Paths aspnet_Paths2 { get; set; }
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual aspnet_Users aspnet_Users1 { get; set; }
        public virtual aspnet_Users aspnet_Users2 { get; set; }
    }
}
