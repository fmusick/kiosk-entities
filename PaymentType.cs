using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kiosk.Models
{
    public partial class PaymentType
    {
        public PaymentType()
        {
            this.FacilityProductFees = new List<FacilityProductFee>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public virtual ICollection<FacilityProductFee> FacilityProductFees { get; set; }

    }
}
