using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class SoftwareVersion
    {
        public int id { get; set; }
        public Nullable<System.DateTime> released_on { get; set; }
        public string assembly_version { get; set; }
        public string download_url { get; set; }
    }
}
