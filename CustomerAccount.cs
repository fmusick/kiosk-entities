using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class CustomerAccount
    {
        public CustomerAccount()
        {
            this.aspnet_Users = new List<aspnet_Users>();
            this.DirectPayPhoneNumbers = new List<DirectPayPhoneNumber>();
        }

        public int id { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public string address_1 { get; set; }

        public string address_2 { get; set; }

        public string city { get; set; }

        public string state { get; set; }

        public string country { get; set; }

        public string zip { get; set; }

        public string account_pin { get; set; }

        public string email { get; set; }

        public string phone_number { get; set; }

        public Nullable<System.DateTime> entry_date { get; set; }

        public Nullable<System.DateTime> last_activity { get; set; }

        public string time_to_contact { get; set; }

        public virtual ICollection<aspnet_Users> aspnet_Users { get; set; }

        public virtual ICollection<DirectPayPhoneNumber> DirectPayPhoneNumbers { get; set; }

    }
}
