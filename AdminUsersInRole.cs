using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class AdminUsersInRole
    {
        public System.Guid RoleId { get; set; }
        public System.Guid UserId { get; set; }
        public string Username { get; set; }
        public string Rolename { get; set; }
        public string ApplicationName { get; set; }
    }
}
