using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class KioskAlert
    {
        public int BaseEvent_id { get; set; }

        public int koisk_id { get; set; }

        public virtual Kiosk Kiosk { get; set; }

        public virtual BaseEvent BaseEvent { get; set; }
    }
}
