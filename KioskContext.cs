using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Kiosk.Models.Mapping;

namespace Kiosk.Models
{
    public partial class KioskContext : DbContext
    {
        static KioskContext()
        {
            Database.SetInitializer<KioskContext>(null);
        }

        public KioskContext() : base("Name=KioskContext") { }

        public DbSet<AdministrativeChangeEvent> AdministrativeChangeEvents { get; set; }
        public DbSet<AdminRoles> AdminRoles { get; set; }
        public DbSet<AdminUsers> AdminUsers { get; set; }
        public DbSet<AdminUsersInRole> AdminUsersInRoles { get; set; }
        public DbSet<aspnet_Applications> aspnet_Applications { get; set; }
        public DbSet<aspnet_Membership> aspnet_Membership { get; set; }
        public DbSet<aspnet_Paths> aspnet_Paths { get; set; }
        public DbSet<aspnet_PersonalizationAllUsers> aspnet_PersonalizationAllUsers { get; set; }
        public DbSet<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public DbSet<aspnet_Profile> aspnet_Profile { get; set; }
        public DbSet<aspnet_Roles> aspnet_Roles { get; set; }
        public DbSet<aspnet_SchemaVersions> aspnet_SchemaVersions { get; set; }
        public DbSet<aspnet_Users> aspnet_Users { get; set; }
        public DbSet<aspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }
        public DbSet<aspnet_WebEvent_Events> aspnet_WebEvent_Events { get; set; }
        public DbSet<BailDeposit> BailDeposits { get; set; }
        public DbSet<BaseEvent> BaseEvents { get; set; }
        public DbSet<CreditCardTransaction> CreditCardTransactions { get; set; }
        public DbSet<CustomerAccount> CustomerAccounts { get; set; }
        public DbSet<DebitCard> DebitCards { get; set; }
        public DbSet<DebitCardBatch> DebitCardBatches { get; set; }
        public DbSet<DebitCardBatchHistoryItem> DebitCardBatchHistoryItems { get; set; }
        public DbSet<DebitCardInventoryEmailAddress> DebitCardInventoryEmailAddresses { get; set; }
        public DbSet<DebitCardOrder> DebitCardOrders { get; set; }
        public DbSet<DebitCardPurchase> DebitCardPurchases { get; set; }
        public DbSet<DirectPayPhoneNumber> DirectPayPhoneNumbers { get; set; }

        //public DbSet<Facility> Facilities { get; set; }
        //public DbSet<FacilityAddress> FacilityAddresses { get; set; }
        //public DbSet<FacilityContact> FacilityContacts { get; set; }

        public DbSet<FacilityInmateEvent> FacilityInmateEvents { get; set; }
        public DbSet<FacilityProductFee> FacilityProductFees { get; set; }
        public DbSet<FacilityProduct> FacilityProducts { get; set; }

        //public DbSet<Fee> Fees { get; set; }

        public DbSet<FeeType> FeeTypes { get; set; }
        public DbSet<FF2XMLSites> FF2XMLSites { get; set; }
        public DbSet<Incarceration> Incarcerations { get; set; }
        public DbSet<Inmate> Inmates { get; set; }
        public DbSet<InmateBail> InmateBails { get; set; }
        public DbSet<Kiosk> Kiosks { get; set; }
        public DbSet<KioskAlert> KioskAlerts { get; set; }
        public DbSet<KioskUpdateSite> KioskUpdateSites { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<NexusInstance> NexusInstances { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<PaymentGatewayType> PaymentGatewayTypes { get; set; }
        public DbSet<PINDebitDeposit> PINDebitDeposits { get; set; }
        public DbSet<PINDebitOrder> PINDebitOrders { get; set; }
        public DbSet<PrepaidCollectDeposit> PrepaidCollectDeposits { get; set; }
        public DbSet<PrepaidCollectOrder> PrepaidCollectOrders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<SoftwareVersion> SoftwareVersions { get; set; }
        public DbSet<TrustDeposit> TrustDeposits { get; set; }
        public DbSet<TrustDepositOrder> TrustDepositOrders { get; set; }
        public DbSet<User_Facility> User_Facility { get; set; }
        public DbSet<DebitCardTransaction> DebitCardTransactions { get; set; }
        public DbSet<PinDepositTransaction> PinDepositTransactions { get; set; }
        public DbSet<PrepaidCollectTransaction> PrepaidCollectTransactions { get; set; }
        public DbSet<TrustDepositTransaction> TrustDepositTransactions { get; set; }
        public DbSet<vw_aspnet_Applications> vw_aspnet_Applications { get; set; }
        public DbSet<vw_aspnet_MembershipUsers> vw_aspnet_MembershipUsers { get; set; }
        public DbSet<vw_aspnet_Profiles> vw_aspnet_Profiles { get; set; }
        public DbSet<vw_aspnet_Roles> vw_aspnet_Roles { get; set; }
        public DbSet<vw_aspnet_Users> vw_aspnet_Users { get; set; }
        public DbSet<vw_aspnet_UsersInRoles> vw_aspnet_UsersInRoles { get; set; }
        public DbSet<vw_aspnet_WebPartState_Paths> vw_aspnet_WebPartState_Paths { get; set; }
        public DbSet<vw_aspnet_WebPartState_Shared> vw_aspnet_WebPartState_Shared { get; set; }
        public DbSet<vw_aspnet_WebPartState_User> vw_aspnet_WebPartState_User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<DirectPayPhoneNumberWithBalance>();
            modelBuilder.Ignore<CustomerAccountWithBalance>();

            modelBuilder.Configurations.Add(new AdministrativeChangeEventMap());
            modelBuilder.Configurations.Add(new AdminRolesMap());
            modelBuilder.Configurations.Add(new AdminUsersMap());
            modelBuilder.Configurations.Add(new AdminUsersInRoleMap());
            modelBuilder.Configurations.Add(new aspnet_ApplicationsMap());
            modelBuilder.Configurations.Add(new aspnet_MembershipMap());
            modelBuilder.Configurations.Add(new aspnet_PathsMap());
            modelBuilder.Configurations.Add(new aspnet_PersonalizationAllUsersMap());
            modelBuilder.Configurations.Add(new aspnet_PersonalizationPerUserMap());
            modelBuilder.Configurations.Add(new aspnet_ProfileMap());
            modelBuilder.Configurations.Add(new aspnet_RolesMap());
            modelBuilder.Configurations.Add(new aspnet_SchemaVersionsMap());
            modelBuilder.Configurations.Add(new aspnet_UsersMap());
            modelBuilder.Configurations.Add(new aspnet_UsersInRolesMap());
            modelBuilder.Configurations.Add(new aspnet_WebEvent_EventsMap());
            modelBuilder.Configurations.Add(new BailDepositMap());
            modelBuilder.Configurations.Add(new BaseEventMap());
            modelBuilder.Configurations.Add(new CreditCardTransactionMap());
            modelBuilder.Configurations.Add(new CustomerAccountMap());
            modelBuilder.Configurations.Add(new DebitCardMap());
            modelBuilder.Configurations.Add(new DebitCardBatchMap());
            modelBuilder.Configurations.Add(new DebitCardBatchHistoryItemMap());
            modelBuilder.Configurations.Add(new DebitCardInventoryEmailAddressMap());
            modelBuilder.Configurations.Add(new DebitCardOrderMap());
            modelBuilder.Configurations.Add(new DebitCardPurchaseMap());
            modelBuilder.Configurations.Add(new DirectPayPhoneNumberMap());

            // modelBuilder.Configurations.Add(new FacilityMap());
            // modelBuilder.Configurations.Add(new FacilityAddressMap());
            // modelBuilder.Configurations.Add(new FacilityContactMap());
            
            modelBuilder.Configurations.Add(new FacilityInmateEventMap());
            modelBuilder.Configurations.Add(new FacilityProductFeeMap());
            modelBuilder.Configurations.Add(new FacilityProductMap());

            // modelBuilder.Configurations.Add(new FeeMap());
             
            modelBuilder.Configurations.Add(new FeeTypeMap());
            modelBuilder.Configurations.Add(new FF2XMLSitesMap());
            modelBuilder.Configurations.Add(new IncarcerationMap());
            modelBuilder.Configurations.Add(new InmateMap());
            modelBuilder.Configurations.Add(new InmateBailMap());
            modelBuilder.Configurations.Add(new KioskMap());
            modelBuilder.Configurations.Add(new KioskAlertMap());
            modelBuilder.Configurations.Add(new KioskUpdateSiteMap());
            modelBuilder.Configurations.Add(new LocationMap());
            modelBuilder.Configurations.Add(new NexusInstanceMap());
            modelBuilder.Configurations.Add(new PaymentGatewayTypeMap());
            modelBuilder.Configurations.Add(new PaymentTypeMap());
            modelBuilder.Configurations.Add(new PINDebitDepositMap());
            modelBuilder.Configurations.Add(new PINDebitOrderMap());
            modelBuilder.Configurations.Add(new PrepaidCollectDepositMap());
            modelBuilder.Configurations.Add(new PrepaidCollectOrderMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new SoftwareVersionMap());
            modelBuilder.Configurations.Add(new TrustDepositMap());
            modelBuilder.Configurations.Add(new TrustDepositOrderMap());
            modelBuilder.Configurations.Add(new User_FacilityMap());
            modelBuilder.Configurations.Add(new DebitCardTransactionMap());
            modelBuilder.Configurations.Add(new PinDepositTransactionMap());
            modelBuilder.Configurations.Add(new PrepaidCollectTransactionMap());
            modelBuilder.Configurations.Add(new TrustDepositTransactionMap());
            modelBuilder.Configurations.Add(new vw_aspnet_ApplicationsMap());
            modelBuilder.Configurations.Add(new vw_aspnet_MembershipUsersMap());
            modelBuilder.Configurations.Add(new vw_aspnet_ProfilesMap());
            modelBuilder.Configurations.Add(new vw_aspnet_RolesMap());
            modelBuilder.Configurations.Add(new vw_aspnet_UsersMap());
            modelBuilder.Configurations.Add(new vw_aspnet_UsersInRolesMap());
            modelBuilder.Configurations.Add(new vw_aspnet_WebPartState_PathsMap());
            modelBuilder.Configurations.Add(new vw_aspnet_WebPartState_SharedMap());
            modelBuilder.Configurations.Add(new vw_aspnet_WebPartState_UserMap());
        }
    }
}
