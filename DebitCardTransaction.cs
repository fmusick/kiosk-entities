using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DebitCardTransaction
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> PurchasedOn { get; set; }
        public string PurchaseMethod { get; set; }
        public string TransactionId { get; set; }
        public Nullable<int> BillCount { get; set; }
        public Nullable<decimal> HandlingFee { get; set; }
        public Nullable<decimal> Surcharge { get; set; }
        public string CreditAuthorization { get; set; }
        public Nullable<System.DateTime> CreditTransactionDate { get; set; }
        public string CreditTransactionId { get; set; }
        public Nullable<decimal> CreditTransactionAmount { get; set; }
        public Nullable<decimal> CreditTransactionFee { get; set; }
        public string Inmate { get; set; }
        public string UserName { get; set; }

        // public virtual Scidyn.Models.Facility Facility { get; set; }

        public string Outlet { get; set; }
        public string CallerName { get; set; }
        public string CallerEmail { get; set; }
        public string CallerPhone { get; set; }
        public string CardId { get; set; }
        public decimal CardValue { get; set; }
    }
}
