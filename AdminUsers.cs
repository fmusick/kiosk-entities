using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kiosk.Models
{
    public partial class AdminUsers
    {
        [Key]
        public System.Guid UserId { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime CreationDate { get; set; }

    }
}
