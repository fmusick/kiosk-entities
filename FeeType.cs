using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kiosk.Models
{
    public partial class FeeType
    {
        public FeeType()
        {
            this.FacilityProductFees = new List<FacilityProductFee>();
        }

        public int id { get; set; }
        public string name { get; set; }

        public virtual ICollection<FacilityProductFee> FacilityProductFees { get; set; }
    }
}
