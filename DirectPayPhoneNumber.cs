using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class DirectPayPhoneNumber
    {
        public int id { get; set; }

        public int customeraccount_id { get; set; }

        public string phone_number { get; set; }

        public string description { get; set; }

        public virtual CustomerAccount CustomerAccount { get; set; }

        /*
        public int facility_id { get; set; }
        public decimal balance { get; set; }
        public virtual Facility Facility { get; set; }
        */
    }
}
