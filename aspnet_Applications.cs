using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class aspnet_Applications
    {
        public aspnet_Applications()
        {
            this.aspnet_Membership = new List<aspnet_Membership>();
            this.aspnet_Membership1 = new List<aspnet_Membership>();
            this.aspnet_Membership2 = new List<aspnet_Membership>();
            this.aspnet_Paths = new List<aspnet_Paths>();
            this.aspnet_Paths1 = new List<aspnet_Paths>();
            this.aspnet_Paths2 = new List<aspnet_Paths>();
            this.aspnet_Roles = new List<aspnet_Roles>();
            this.aspnet_Roles1 = new List<aspnet_Roles>();
            this.aspnet_Roles2 = new List<aspnet_Roles>();
            this.aspnet_Users = new List<aspnet_Users>();
            this.aspnet_Users1 = new List<aspnet_Users>();
            this.aspnet_Users2 = new List<aspnet_Users>();
        }

        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public System.Guid ApplicationId { get; set; }
        public string Description { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership1 { get; set; }
        public virtual ICollection<aspnet_Membership> aspnet_Membership2 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths1 { get; set; }
        public virtual ICollection<aspnet_Paths> aspnet_Paths2 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles1 { get; set; }
        public virtual ICollection<aspnet_Roles> aspnet_Roles2 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users1 { get; set; }
        public virtual ICollection<aspnet_Users> aspnet_Users2 { get; set; }
    }
}
