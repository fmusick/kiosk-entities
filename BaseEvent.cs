using System;
using System.Collections.Generic;

namespace Kiosk.Models
{
    public partial class BaseEvent
    {
        public int id { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public System.DateTime occurred { get; set; }
        public virtual AdministrativeChangeEvent AdministrativeChangeEvent { get; set; }
        public virtual KioskAlert KioskAlert { get; set; }
        public virtual Kiosk Kiosk { get; set; }
    }
}
