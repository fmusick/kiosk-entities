using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kiosk.Models
{
    public partial class FacilityProductFee
    {
        public int id { get; set; }
        public int facility_product_id { get; set; }
        public int fee_type_id { get; set; }
        public int payment_type_id { get; set; }
        public decimal value { get; set; }

        public virtual FacilityProduct FacilityProduct { get; set; }
        public virtual PaymentType PaymentType { get; set; }
        public virtual FeeType FeeType { get; set; }
    }
}
